import numpy as np
from NSNN.Layer_Dense import Layer_Dense
from NSNN.Activation_Functions import Activation_ReLU, Activation_Softmax
from NSNN.Loss_CCE import Loss_CCE
from NSNN.Optimization_Functions import Adaptative_Gradient, Adaptative_Momentum, Root_Mean_Square_Propagation, Stochastic_Gradient_Descent

class Neural_Network(object):

    """Creates the Neural_Network for the simmulation"""

    def __init__(self,_dna):
        """Initializes the Neural Network

        :_inputs: TODO

        """
        self.dna = _dna
        self.layer_dense = {}
        self.act_relu = {}

        for layer in range(len(_dna['network'])-2):
            self.layer_dense['layer{:d}'.format(layer+1)] = Layer_Dense(_dna['network'][layer],_dna['network'][layer+1])
            self.act_relu['layer{:d}'.format(layer+1)] = Activation_ReLU()

        self.last_layer = Layer_Dense(_dna['network'][-2],
                                      _dna['network'][-1]
                                     )
        self.act_softmax= Activation_Softmax()
        self.loss_function = Loss_CCE()
        if _dna['learning_method'] == 'SGD':
            self.optimizer = Stochastic_Gradient_Descent(_dna['learning_rate'],
                                                         _dna['learning_decay'],
                                                         _dna['learning_momentum']
                                                        )
        elif _dna['learning_method'] == 'AdaGra':
            self.optimizer = Adaptative_Gradient(_dna['learning_rate'],
                                                 _dna['learning_decay'],
                                                 _dna['learning_epsilon']
                                                )
        elif _dna['learning_method'] == 'RMSProp':
            # Does not work
            self.optimizer = Root_Mean_Square_Propagation(_dna['learning_rate'],
                                                          _dna['learning_decay'],
                                                          _dna['learning_epsilon'],
                                                          _dna['learning_rho']
                                                         )
        elif _dna['learning_method'] == 'Adam':
            self.optimizer = Adaptative_Momentum(_dna['learning_rate'],
                                                 _dna['learning_decay'],
                                                 _dna['learning_epsilon'],
                                                 _dna['learning_beta_1'],
                                                 _dna['learning_beta_2']
                                                )

    def reset(self):
        """Resets the Neural Network current parameters to birth
        """
        self.optimizer.reset()

    def forward(self, _inputs):
        """Performs a forward step

        :_inputs: Numpy Array
        :_target: Numpy Array

        """
        curr_data = _inputs
        for layer in self.layer_dense:
            self.layer_dense[layer].forward(curr_data)
            self.act_relu[layer].forward(self.layer_dense[layer].output)
            curr_data = self.act_relu[layer].output

        self.last_layer.forward(curr_data)
        self.act_softmax.forward(self.last_layer.output)
        self.output = self.act_softmax.output

    def backward(self):
        """Perform the Back Propagation
        """
        samples = len(self.output)
        if len(self.target.shape) == 2:
            self.target = np.argmax(self._target,axis=1)

        self.dinputs = self.output.copy()
        self.dinputs[range(samples),self.target] -= 1
        self.dinputs = self.dinputs / samples

        self.last_layer.backward(self.dinputs)
        dinputs = self.last_layer.dinputs
        for ii in np.arange(len(self.dna['network'])-2,0,-1):
            key = 'layer{:d}'.format(ii)
            self.act_relu[key].backward(dinputs)
            self.layer_dense[key].backward(self.act_relu[key].dinputs)
            dinputs = self.layer_dense[key].dinputs

    def performance(self, _target):
        """Perform the loss and accuracy calculation

        :_target: Numpy Array

        """
        self.target = _target
        self.loss = self._loss_calculation()
        self.accuracy = self._accuracy_calculation()

    def _loss_calculation(self):
        """Compute the loss of the prediction and saves the target data

        :returns: Float

        """
        return self.loss_function.calculate(self.output,self.target)

    def _accuracy_calculation(self):
        """Compute the accuracy of the prediction

        :returns: Float

        """
        predictions = np.argmax(self.output,axis=1)
        if len(self.target.shape) == 2:
            self.target = np.argmax(self.target,axis=1)
    
        return np.mean(predictions == self.target)

    def optimization(self):
        """Perform the optimization of each layer
        """
        self.optimizer.pre_update_params()
        for layer in self.layer_dense:
            self.optimizer.update_params(self.layer_dense[layer])

        self.optimizer.update_params(self.last_layer)
        self.optimizer.post_update_params()
