import numpy as np
import matplotlib.pyplot as plt

class Holder(object):

    """Creates a Holder in 2D space from a list of 4 points"""

    def __init__(self, _n):
        """Initialize the Holder

        :_n: integer

        """ 
        self.holdlines = np.zeros((_n,4))
        self.len = _n

    def add_holdline(self, _fix_points):
        """Creates a new holdline

        :_fix_points: Numpy array

        """
        for ii in range(self.len):
            if np.count_nonzero(self.holdlines[ii,:]) == 0:
                self.holdlines[ii,:] = _fix_points
                break

    def plot(self, _fig = None, _ax = None):
        """Plot the Holder

        :_fig: TODO
        :_ax: TODO

        """
        if not _fig:
            for ii in range(self.len):
                x = self.holdlines[ii,[0,2]]
                y = self.holdlines[ii,[1,3]]
                plt.plot(x,y,color='tab:orange')
        else:
            for ii in range(self.len):
                x = self.holdlines[ii,[0,2]]
                y = self.holdlines[ii,[1,3]]
                
                _ax.plot(x,y,color='tab:orange')

if __name__ == '__main__':
    h = Holder(2)
    h.add_holdline(np.array([0.5,0.14,1,1]))
    h.add_holdline(np.array([0.5,0.14,0,1]))
    h.plot()
    plt.show()
