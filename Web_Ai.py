import numpy as np
import matplotlib.pyplot as plt

from Functions import Functions
from Neural_Network import Neural_Network

import nnfs
from nnfs.datasets import spiral_data


if __name__ == '__main__':
    nnfs.init()
    
    x, y = spiral_data(samples=100,classes=3)
    x_test, y_test = spiral_data(samples=100,classes=3)
    dna = {'network': np.array([2,64,3]),
           'learning_method': 'Adam',
           'learning_rate': 5e-2,
           'learning_decay': 5e-7,
           'learning_momentum': 0.9,
           'learning_epsilon': 1e-7,
           'learning_rho': 0.9,
           'learning_beta_1': 0.9,
           'learning_beta_2': 0.999
          }
    
    NN = Neural_Network(dna)

    ep = []
    acc = []
    loss = []
    rate = []

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)

    for epoch in range(10001):
        NN.forward(x)
        NN.performance(y)
        ep.append(epoch)
        acc.append(NN.accuracy)
        loss.append(NN.loss)
        rate.append(NN.optimizer.current_learning_rate)
        NN.backward()
        NN.optimization()

    ax1.plot(ep,acc,label='accuracy',color='green')
    ax2.plot(ep,loss,label='loss',color='red')
    ax3.plot(ep,rate,label='learning rate',color='blue')
    plt.savefig('performance.png',format='png',dpi=900)

    NN.reset()

#    inp = np.array([[-1]])
#    network = np.array([1,3,2])
#    
#    NN = Neural_Network(network)
#    NN.forward(inp)

#    plt.scatter(x[:,0], x[:,1],c=y,cmap='brg')
#    plt.show()
    
#    F = Functions()
#
#    n = 2
#    lines = (('a','b'),
#             ('a','c')
#            )
#    lines_dict = {'a': np.array([0.5,0.14]),
#                  'b': np.array([1,1]),
#                  'c': np.array([0,1])
#                 }
#    wall = F.create_holder(n,lines,lines_dict)
#
#    NN = Neural_Network(1,2)
#    NN.forward(np.array([-0.5]),np.array([0,1]))
