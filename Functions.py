import numpy as np
from Holder import Holder as H

class Functions(object):

    """General Functions Class"""

    def create_holder(self, _n, _lines, _lines_dict):
        """Creates a new holder of n lines
    
        :_n: int
        :_lines: tupple of strings of _lines_dict keys
        :_lines_dict: dictionary with Numpy arrays
        :returns: Holder object
    
        """
        wall = H(_n)
        for line in _lines:
            wall.add_holdline(np.concatenate((_lines_dict[line[0]],
                                              _lines_dict[line[1]]
                                             )))
    
        return wall
