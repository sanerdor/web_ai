import numpy as np

class Layer_Dense(object):

    """Creates the cornerstone of the Neural Network"""

    def __init__(self, _n_inputs, _n_neurons):
        """Initializes the Neural Network

        :_n_inputs: integer
        :_n_neurons: integer

        """
        self.weights = 0.01*np.random.randn(_n_inputs, _n_neurons)
        self.biases = np.zeros((1,_n_neurons))

    def forward(self, _inputs):
        """Computes an output from the _inputs

        :_inputs: Undefined

        """
        self.inputs = _inputs
        self.output = np.dot(_inputs, self.weights)+self.biases

    def backward(self, _dvalues):
        """Apply the Back Propagation algorithm to correct the wrights and biases

        :_dvalues: TODO

        """
        self.dweights = np.dot(self.inputs.T, _dvalues)
        self.dbiases = np.sum(_dvalues, axis=0, keepdims=True)
        self.dinputs = np.dot(_dvalues, self.weights.T)
