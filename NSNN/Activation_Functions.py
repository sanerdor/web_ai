import numpy as np

class Activation_ReLU(object):

    """Creates functions for the Activation ReLU"""

    def forward(self, _inputs):
        """Computes an output from the _inputs

        :_inputs: Undefined

        """
        self.inputs = _inputs
        self.output = np.maximum(0,_inputs)

    def backward(self, _dvalues):
        """Applies a Back Propagation

        :_dvalues: Numpy Array

        """
        self.dinputs = _dvalues.copy()
        self.dinputs[self.inputs <= 0] = 0

class Activation_Softmax(object):

    """Creates functions for the Activation Softmax"""

    def forward(self, _inputs):
        """Computes an output from the _inputs

        :_inputs: Undefined

        """
        self.inputs = _inputs
        exp_values = np.exp(_inputs-np.max(_inputs,axis=1,keepdims=True))
        probabilities = exp_values/np.sum(exp_values,axis=1,keepdims=True)
        self.output = probabilities

    def backward(self, _dvalues):
        """Computes the Back Propagation

        :_dvalues: TODO

        """
        self.dinputs = np.empty_like(_dvalues)
        for index, (single_output, single_dvalues) in enumerate(zip(self.output, _dvalues)):
            single_output = single_output.reshape(-1,1)
            jacobian_matrix = np.diagflat(single_output)-np.dot(single_output,single_output.T)
            self.dinputs[index] = np.dot(jacobian_matrix,single_dvalues)
