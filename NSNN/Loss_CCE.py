import numpy as np

class Loss_CCE(object):

    """Creates functions for the calculation of Loss"""

    def calculate(self, _output, _target):
        """Calculate the Loss

        :_output: TODO
        :_y: TODO
        :returns: Numpy Array

        """
        sample_losses = self.forward(_output, _target)
        data_loss = np.mean(sample_losses)
        
        return data_loss

    def forward(self, _output, _target):
        """Computes a forward step

        :_y_pred: Numpy Array
        :_y_true: Numpy Array
        :returns: Numpy Array

        """
        samples = len(_output)
        output_clipped = np.clip(_output, 1e-7, 1-1e-7)
        if len(_target.shape) == 1:
            correct_confidences = output_clipped[range(samples),_target]
        elif len(_target.shape) == 2:
            correct_confidences = np.sum(output_clipped*_target,axis=1)

        negative_log = -np.log(correct_confidences)

        return negative_log

    def backward(self, _dvalues, _target):
        """Computes the Back Propagation

        :_dvalues: TODO
        :_y_true: TODO
        :returns: TODO

        """
        samples = len(_dvalues)
        labels = len(_dvalues[0])

        if len(_target.shape) == 1:
            _target = np.eye(labels)[_target]

        self.dinputs = -_target/_dvalues
        self.dinputs = self.dinputs/samples
