import numpy as np

class Stochastic_Gradient_Descent(object):

    """Creates functions for the Stochastic Gradient Descent optimization"""

    def __init__(self, _learning_rate=1.0, _decay=0., _momentum=0.):
        """Initializes the SGD optimizer

        :_learning_rate: TODO

        """
        self.learning_rate = _learning_rate
        self.current_learning_rate = _learning_rate
        self.decay = _decay
        self.momentum = _momentum
        self.iterations = 0

    def pre_update_params(self):
        """Update the learning rate
        """
        if self.decay:
            self.current_learning_rate = self.learning_rate \
                    *(1./(1.+self.decay*self.iterations))

    def update_params(self, _layer):
        """Updates all the weights and biases of target _layer

        :_layer: TODO

        """
        if self.momentum:
            if not hasattr(_layer,'weight_momentums'):
                _layer.weight_momentums = np.zeros_like(_layer.weights)
                _layer.bias_momentums = np.zeros_like(_layer.biases)
                
            weight_updates = self.momentum*_layer.weight_momentums \
                    -self.current_learning_rate*_layer.dweights
            _layer.weight_momentums = weight_updates
            bias_updates = self.momentum*_layer.bias_momentums \
                    -self.current_learning_rate*_layer.dbiases
            _layer.bias_momentums = bias_updates
        else:
            weight_updates = -self.current_learning_rate*_layer.dweights
            bias_updates = -self.current_learning_rate*_layer.dbiases

        _layer.weights += weight_updates
        _layer.biases += bias_updates

    def post_update_params(self):
        """Update the iteration
        """
        self.iterations += 1

    def reset(self):
        """Resets the learning data
        """
        self.current_learning_rate = self.learning_rate
        self.iterations = 0

class Adaptative_Gradient(object):

    """Creates functions for the Stochastic Gradient Descent optimization"""

    def __init__(self, _learning_rate=1., _decay=0., _epsilon=1e-7):
        """Initializes the SGD optimizer

        :_learning_rate: TODO

        """
        self.learning_rate = _learning_rate
        self.current_learning_rate = _learning_rate
        self.decay = _decay
        self.epsilon = _epsilon
        self.iterations = 0

    def pre_update_params(self):
        """Update the learning rate
        """
        if self.decay:
            self.current_learning_rate = self.learning_rate*(1./(1.+self.decay*self.iterations))

    def update_params(self, _layer):
        """Updates all the weights and biases of target _layer

        :_layer: TODO

        """
        if not hasattr(_layer, 'weight_cache'):
            _layer.weight_cache = np.zeros_like(_layer.weights)
            _layer.bias_cache = np.zeros_like(_layer.biases)

        _layer.weight_cache += _layer.dweights**2
        _layer.bias_cache += _layer.dbiases**2

        _layer.weights += -self.current_learning_rate*_layer.dweights \
                / (np.sqrt(_layer.weight_cache)+self.epsilon)
        _layer.biases += -self.current_learning_rate*_layer.dbiases \
                / (np.sqrt(_layer.bias_cache)*self.epsilon)

    def post_update_params(self):
        """Update the iteration
        """
        self.iterations += 1

    def reset(self):
        """Resets the learning data
        """
        self.current_learning_rate = self.learning_rate
        self.iterations = 0

class Root_Mean_Square_Propagation(object):

    """Creates functions for the Stochastic Gradient Descent optimization"""

    def __init__(self, _learning_rate=0.001, _decay=0., _epsilon=1e-7, _rho=0.9):
        """Initializes the SGD optimizer

        :_learning_rate: TODO

        """
        self.learning_rate = _learning_rate
        self.current_learning_rate = _learning_rate
        self.decay = _decay
        self.epsilon = _epsilon
        self.rho = _rho
        self.iterations = 0
    
    def pre_update_params(self):
        """Update the learning rate
        """
        if self.decay:
            self.current_learning_rate = self.learning_rate \
            *(1./(1.+self.decay*self.iterations))

    def update_params(self, _layer):
        """Updates all the weights and biases of target _layer

        :_layer: TODO

        """
        if not hasattr(_layer, 'weight_cache'):
            _layer.weight_cache = np.zeros_like(_layer.weights)
            _layer.bias_cache = np.zeros_like(_layer.biases)

        _layer.weight_cache = self.rho*_layer.weight_cache \
                +(1.-self.rho)*_layer.dweights**2
        _layer.bias_cache = self.rho*_layer.bias_cache \
                +(1.-self.rho)*_layer.dbiases**2
        _layer.weights += -self.current_learning_rate*_layer.dweights \
                / (np.sqrt(_layer.weight_cache)+self.epsilon)
        _layer.biases += -self.current_learning_rate*_layer.dbiases \
                / (np.sqrt(_layer.bias_cache)*self.epsilon)

    def post_update_params(self):
        """Update the iteration
        """
        self.iterations += 1

    def reset(self):
        """Resets the learning data
        """
        self.current_learning_rate = self.learning_rate
        self.iterations = 0

class Adaptative_Momentum(object):

    """Creates functions for the Stochastic Gradient Descent optimization"""

    def __init__(self, _learning_rate=0.001, _decay=0., _epsilon=1e-7,\
                 _beta_1=0.9, _beta_2=0.999):
        """Initializes the SGD optimizer

        :_learning_rate: TODO

        """
        self.learning_rate = _learning_rate
        self.current_learning_rate = _learning_rate
        self.decay = _decay
        self.epsilon = _epsilon
        self.beta_1 = _beta_1
        self.beta_2 = _beta_2
        self.iterations = 0
    
    def pre_update_params(self):
        """Update the learning rate
        """
        if self.decay:
            self.current_learning_rate = self.learning_rate \
            *(1./(1.+self.decay*self.iterations))

    def update_params(self, _layer):
        """Updates all the weights and biases of target _layer

        :_layer: TODO

        """
        if not hasattr(_layer, 'weight_cache'):
            _layer.weight_momentums = np.zeros_like(_layer.weights)
            _layer.weight_cache = np.zeros_like(_layer.weights)
            _layer.bias_momentums = np.zeros_like(_layer.biases)
            _layer.bias_cache = np.zeros_like(_layer.biases)

        _layer.weight_momentums = self.beta_1*_layer.weight_momentums \
                +(1.0-self.beta_1)*_layer.dweights
        _layer.bias_momentums = self.beta_1*_layer.bias_momentums \
                +(1.0-self.beta_1)*_layer.dbiases

        weight_momentums_corrected = _layer.weight_momentums \
                /(1.0-self.beta_1**(self.iterations+1))
        bias_momentums_corrected = _layer.bias_momentums \
                /(1.0-self.beta_1**(self.iterations+1))

        _layer.weight_cache = self.beta_2*_layer.weight_cache \
                +(1.0-self.beta_2)*_layer.dweights**2
        _layer.bias_cache = self.beta_2*_layer.bias_cache \
                +(1.0-self.beta_2)*_layer.dbiases**2
    
        weight_cache_corrected = _layer.weight_cache \
                /(1.0-self.beta_2**(self.iterations+1))
        bias_cache_corrected = _layer.bias_cache \
                /(1.0-self.beta_2**(self.iterations+1))

        _layer.weights += -self.current_learning_rate \
                *weight_momentums_corrected \
                /(np.sqrt(weight_cache_corrected)+self.epsilon)
        _layer.biases += -self.current_learning_rate \
                *bias_momentums_corrected \
                /(np.sqrt(bias_cache_corrected)+self.epsilon)

    def post_update_params(self):
        """Update the iteration
        """
        self.iterations += 1

    def reset(self):
        """Resets the learning data
        """
        self.current_learning_rate = self.learning_rate
        self.iterations = 0
